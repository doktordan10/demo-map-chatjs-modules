const app = frontexpress();
var librariesloaded = {},
    pagesloaded = {},
    pagesContent = {},
    polygons1 = [],
    building,
    selected = null,
    buildingJson,
    highlightSearch = '',
    originalColor,
    originalOpacity,
    filterParams = '',
    projectData,
    myLayer,
    osm,
    map;
	// front-end logic on navigation path root
  //$(document).ready(function(){
    //load nav into index as a module
    var navDetail = {
                    htmlRoute:"/modules/navigationBar/navigationBar.html",
                    jsRoute : ["/modules/navigationBar/navigationBar.js"],
                    container: "navigation-bar",
                    modules : [],
                    page:"navigationBar"
                  }

    loadPageContent(navDetail);
 // });

    app.get('/', (req, res) => {
      var pageDetail = {
                        htmlRoute:"/pages/mapPage/mapPage.html",
                        jsRoute : ["/pages/mapPage/mapPage.js"],
                        modules : [
                          {modulename:"filter-buttons"},
                          {modulename:"map-leaflet"}
                        ],
                        container:"#content",
                        page:"mapPage",
                        route:"/",
                        title:"Dashboard"
                      };
      loadPageContent(pageDetail);
      addCssMod('map-page');

    });
    // front-end logic on navigation path "/building detail"
    app.get('/buildingdetail/:id', (req, res) => {
       var buildingdetailroute = req.params.id,
          pageDetail = {
                        htmlRoute:"/pages/buildingDetail/buildingDetail.html",
                        jsRoute : ["/node_modules/smoothscroll-polyfill/dist/smoothscroll.min.js","/pages/buildingDetail/gantt-chart-d3.js","/pages/buildingDetail/buildingDetail.js"],
                        modules : [
                            {modulename:"building-general",param : {buildingNumber:req.params.id}},
                            {modulename:"fci-bar-chart",param : {buildingNumber:req.params.id}},
                            {modulename:"project-list", param: {buildingNumber: req.params.id}},
                            {modulename:"work-order-chart",param : {buildingNumber:req.params.id}},
                            {modulename:"survey-histogram",param : {buildingNumber:req.params.id}},
                        ],
                        container:"#content",
                        page:"buildingDetail",
                        route:"/buildingdetail/"+buildingdetailroute,
                        title:"Building Detail"
                      };
      loadPageContent(pageDetail);
      addCssMod('building-detail');
    });
    app.get("/fci",(req,res)=>{
        var pageDetail = {
                        htmlRoute:"/pages/fci/fci.html",
                        jsRoute : ["/pages/fci/fci.js"],
                        modules : [{modulename:"fci-bar-chart"}],
                        container:"#content",
                        page:"fci",
                        route:"/fci",
                        title:"FCI"
                      }
        loadPageContent(pageDetail);
        addCssMod('fci-page');
    });

    // start front-end application
    app.listen(() => {
      // on DOM ready
      //console.log('App is listening requests');
    });
  window.onpopstate = function(e){
    if(e.state){
      goToURL(window.location.pathname);
    }
  };
  function addCssMod(param){
    $('body').removeClass();
    $('body').addClass(param);
  }
  //adds location to history in order to be able to do backs without refreshing 
  function addHistory(param,title){
     if (history.pushState) {
          var newurl = window.location.protocol + "//" + window.location.host + param;
          window.history.pushState({url:newurl},title,newurl);
      }
  }
  //initiatilize modules inside of routes
 	function loadPageContent(pageDetail){
    if (pageDetail.hasOwnProperty('route')){
            addHistory(pageDetail.route,pageDetail.title);
    }
     if (pageDetail.hasOwnProperty('page')){
      if (pagesloaded[pageDetail.page] != true){
        pagesloaded[pageDetail.page] = true;
         if ($(pageDetail.container).length){
            $(pageDetail.container).load(pageDetail.htmlRoute,function(data, textStatus, jqxhr){
              pagesContent[pageDetail.page] = data;
              scriptsModules(pageDetail);
            })
          }
        }else{
          $(pageDetail.container).html(pagesContent[pageDetail.page]);
          scriptsModules(pageDetail);
        }
     }
 	}
  //initiatilize scripts
  function scriptsModules(pageDetail){
    if (pageDetail.hasOwnProperty('modules')){
      moduleInit(pageDetail);
    }
    if (pageDetail.hasOwnProperty('jsRoute')){
     jsLoader(pageDetail);
    }
  }
  //navigate
 	function goToURL(param){
    if (param[0] != undefined){
        if (param[0] ==  '/'){
          param = param.replace(/^\/+/, '');
        }
    }
    app.httpGet(`/${param}`);
 	}
  //load js if already loaded run cached js
  function jsLoader(pageDetail){
    if (pageDetail.hasOwnProperty('page')){
         var param = '';
         if (pageDetail.hasOwnProperty('param')){
          param = pageDetail.param;
         }
         if (librariesloaded[pageDetail.page] != true){
            librariesloaded[pageDetail.page] = true;
             for (i = 0; i < pageDetail.jsRoute.length; i++) {
                 $.getScript( pageDetail.jsRoute[i], function( data, textStatus, jqxhr ) {
                  if (typeof  window[pageDetail.page] === "function") {
                       window[pageDetail.page](param);
                  }

                 });
              }
          }else{
            window[pageDetail.page](param);
          }
    }
  }
  //sanitize modules
 	function cleanUpModuleName(module){
 		str = module.toCamel();
		if ( typeof console !== 'undefined' ) return str;
 	}
  //initialize module
 	function moduleInit(pageDetail){
   for (i = 0; i < pageDetail.modules.length; i++) {
        var moduleName = cleanUpModuleName(pageDetail.modules[i]['modulename']),
        moduleDetail = {
                        htmlRoute:"/modules/"+moduleName+"/"+moduleName+".html",
                        jsRoute : ["/modules/"+moduleName+"/"+moduleName+".js"],
                        container: pageDetail.modules[i]['modulename'],
                        page:moduleName
                      }
        if (pageDetail.modules[i].hasOwnProperty('param')){
           moduleDetail['param'] = pageDetail.modules[i]['param']
        }
		    loadPageContent(moduleDetail);
		}
 	}

 	String.prototype.toCamel = function(){
    return this.replace(/(\-[a-z])/g, function($1){
      return $1.toUpperCase().replace('-','');
    });
  };
