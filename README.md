## Compiling CSS

SCSS is compiled into CSS using [gulp.js](https://gulpjs.com). You'll need to have the gulp CLI installed:

```npm install gulp-cli -g```

Note: this will install the Gulp CLI globally for this node version. If you ever update to a new node version you'll need install the Gulp CLI again.


Then, make sure you have the dependencies installed. From the project root:

```npm install```

Next you can compile the SCSS:

```gulp sass```

If you want to keep monitoring the file system for changes:

```gulp sass:watch```
