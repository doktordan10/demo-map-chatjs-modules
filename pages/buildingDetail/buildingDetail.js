function buildingDetail(){
  $('.to-top, #fciLink, #projectLink, #workOrderLink, #surveyLink').on('click', function(event) {
      event.preventDefault();
      // use loaded scrollIntoView polyfill for smooth scrolling
      // npm dependency loaded routes.js

      //$($(this).attr('href'))[0].scrollIntoView({ behavior: 'smooth' });

      let destination = $($(this).attr('href')).offset().top;

      window.scroll({ top: destination, left: 0, behavior: 'smooth' });
  });
}
