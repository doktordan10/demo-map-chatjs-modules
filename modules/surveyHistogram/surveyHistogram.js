/**
 * Created by mberger on 3/13/18.
 **/

function surveyHistogram(param) {
    var surveys = [];
    var data = {"results":[{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"3","WO_DATE":"2017-08","WO_YEAR":"2017"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2017-09","WO_YEAR":"2017"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2017-10","WO_YEAR":"2017"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"3","WO_DATE":"2017-11","WO_YEAR":"2017"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2017-11","WO_YEAR":"2017"},{"WO_COUNT":8,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2017-12","WO_YEAR":"2017"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2018-01","WO_YEAR":"2018"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"4","WO_DATE":"2018-03","WO_YEAR":"2018"},{"WO_COUNT":4,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2018-03","WO_YEAR":"2018"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"2","WO_DATE":"2018-04","WO_YEAR":"2018"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"4","WO_DATE":"2018-08","WO_YEAR":"2018"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"4","WO_DATE":"2018-09","WO_YEAR":"2018"},{"WO_COUNT":4,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2018-09","WO_YEAR":"2018"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"4","WO_DATE":"2018-10","WO_YEAR":"2018"},{"WO_COUNT":1,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2018-10","WO_YEAR":"2018"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2018-11","WO_YEAR":"2018"},{"WO_COUNT":8,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2019-01","WO_YEAR":"2019"},{"WO_COUNT":4,"CUSTOMER_SURVEY_RATING":"1","WO_DATE":"2019-02","WO_YEAR":"2019"},{"WO_COUNT":7,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2019-02","WO_YEAR":"2019"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"4","WO_DATE":"2019-03","WO_YEAR":"2019"},{"WO_COUNT":1,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2019-03","WO_YEAR":"2019"},{"WO_COUNT":10,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2019-04","WO_YEAR":"2019"},{"WO_COUNT":4,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2019-05","WO_YEAR":"2019"},{"WO_COUNT":2,"CUSTOMER_SURVEY_RATING":"5","WO_DATE":"2019-07","WO_YEAR":"2019"}]};
        if (data.results.length > 0) {
            surveys = data.results;
            mungeSurveyData(surveys);
        } else {
            $("#surveyErrorMessage").text("Survey Service did not return any data.")
        }
   
    function mungeSurveyData(surveyData) {
        var cols = [], dataCol = [],chartD = {},dateCols =[],dateData = {};
        dataCol.push("Star Rating");
        chartD.cats = ["1 Star","2 Stars","3 Stars","4 Stars","5 Stars"].reverse();
              
        dateData.cats = ["2017-08", "2017-09", "2017-10", "2017-11", "2017-12", "2018-01", "2018-03", "2018-04", "2018-08", "2018-09", "2018-10", "2018-11", "2019-01", "2019-02", "2019-03", "2019-04", "2019-05", "2019-07"];
        dateData.columns = [["One Star", 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 4, 0, 0, 0, 0]
                            ,["Two Star", 0, 0, 0, 0, 0, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                            ,["Three Star", 2, 0, 0, 2, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0, 0]
                            ,["Four Star", 0, 0, 0, 0, 0, 0, 2, 0, 2, 2, 2, 0, 0, 0, 2, 0, 0, 0]
                            ,["Five Star", 0, 2, 2, 2, 8, 2, 4, 0, 0, 4, 1, 2, 8, 7, 1, 10, 4, 2]];
        chartD.columns = [["Star Rating", 59, 10, 4, 2, 4]];
        chartD.cats = ["5 Stars", "4 Stars", "3 Stars", "2 Stars", "1 Star"];
        console.log()
        buildStackedBarChart(dateData,"#surveyOverTimeChart",[["One Star","Two Star","Three Star","Four Star","Five Star"]]);
        buildHistogramChart(chartD,"#surveyHistogramChart");
        function checkDatePosition(date) {
            var position;
            for (var i=0;i<dates.length;i++){
                if (date == dates[i]) {
                    position = i + 1;
                }
            }
            return position;
        }
    }


    function buildStackedBarChart(chartData,elem,groups) {
        var colorBar = ["#D76A73", "#F99662", "#FCC838", "#76BAC8", "#75A580"];
        var ref = {"One":0,"Two":1,"Three":2,"Four":3,"Five":4};
        $(elem).css("height", function (index) {
            return (chartData.columns[0].length * 36) + 30;
        });
        chart = bb.generate({
            data: {
                columns: chartData.columns,
                type: "bar",
                groups:groups,
                color:function(color,d) {
                
                  if (typeof(d["id"]) == "undefined") {
                    var label = d.split(" ")[0];
                    return colorBar[ref[label]];
                  } else if (d["id"].indexOf("Rating") > -1) { // if id contains Rating, we are doing histogram
                    return colorHistogram[d.index];
                  } else {
                    var star = d.id.split(" ")[0];
                    return colorBar[ ref[star] ];
                  }
                }
            },
            legend:{
                hide: true
            },
            bar: {
                width: {
                    ratio: 0.5
                },
                zerobased: true
            },
            axis: {
                rotated: true,
                x: {
                    type: "category",
                    categories: chartData.cats
                }
            },
            tooltip: {
                format: {
                    value: function (value, ratio, id) {
                      if (value > 0) {
                        return value;
                      }
                    }
                }
            },
            bindto: elem
        });
    }//end buildBarChart

  function buildHistogramChart(chartData, elem) {
    var colorBar = ["#D76A73", "#F99662", "#FCC838", "#76BAC8", "#75A580"];
    var colorHistogram = ["#75A580", "#76BAC8", "#FCC838", "#F99662", "#D76A73"];
    var ref = {"One": 0, "Two": 1, "Three": 2, "Four": 3, "Five": 4};
    var ref2 = {0:"Five",1:"Four",2:"Three",3:"Two",4:"One"};
    $(elem).css("height", function (index) {
      return (chartData.columns[0].length * 36) + 30;
    });
    chart = bb.generate({
      data: {
        columns: chartData.columns,
        type: "bar",
        color: function (color, d) {
          if (typeof(d["id"]) == "undefined") {
            var label = d.split(" ")[0];
            return colorBar[ref[label]];
          } else if (d["id"].indexOf("Rating") > -1) {
            // console.log(d.index);
            return colorHistogram[d.index];
          }
        }
      },
      legend: {
        hide: true
      },
      bar: {
        width: {
          ratio: 0.5
        },
        zerobased: true
      },
      axis: {
        rotated: true,
        x: {
          type: "category",
          categories: chartData.cats
        }
      },
      tooltip: {
        format: {
          value: function (value, ratio, id) {
            if (value > 0) {
              return value;
            }
          },
          name: function (name, ratio, id,index) {
            return ref2[index] + " " + name;
          }
        }
      },
      bindto: elem
    });
  }//end buildBarChart
}
