/**
 * Created with PhpStorm.
 * User: michaelberger
 * Date: 2/15/18
 * Time: 4:18 PM
 */
function fciBarChart(param) {


    var usageMap = {
        "Child_Care_Center":"Administrative",
        "Admin_&_Support": "Administrative",
        "Laboratory": "Research",
        "Residential" : "Residential",
        "Research":"Research",
        "Acad_&_Research": "Acad_&_Research",
        "Academic":"Academic",
        "Athletic_Facility":"Other",
        "Other": "Other",
        "Storage": "Other",
        "Parking" : "Other",
        "Fire/Police_Station": "Other",
        "Campus_Activities_Complex": "Campus_Activities_Complex",
        "Multipurpose_Use":"Acad_&_Research"
    };
    var uniqueUsages = findUniqueVals(usageMap);

    var fcichart = {}, buildings = [], buildingsByUse = {}, buildingsByRegion = {}, buildingUseMap = {}, buildingRegions = [], campusFCI = [], buildingDept = {}, buildingDeptList = {},campusAverage = [];
    var buildingUrl = "https://naioya5bdl.execute-api.us-east-1.amazonaws.com/dev/buildingdetail";
    $.ajax({
        type: 'GET',
        url: buildingUrl,
        dataType: 'json',
    }).done(function (data) {
        if (data.length > 0) {
            buildings = data;
            mungeBuildingData(buildings);
        } else {
            alert("Building Service did not return any data.")
        }
    });

    function buildFCIBarChart(columns, options) {
        // console.log("buildFCIBarChart called");
        if (typeof(param.buildingNumber) !== "undefined") {
            options.buildingNumber = param.buildingNumber;
            var fci = buildings.find(function(building) {
                return building.building_number == options.buildingNumber;
            }).fci;
            // console.log(fci);
            columns[0].splice(1,0, twoDecimalRound(fci));
            options.categories.splice(0,0,options.buildingNumber);
            // console.log(columns);
        }
        //create overlay of average
        if (typeof(options.groupLabel) === "undefined") {
            options.groupLabel = "Campus";
        }
        $("#fciAverageText").empty();
        $("#fciAverageText").append("<h6 class=\"fci-average-position\">Average for " + options.groupLabel + ": " + twoDecimalRound(options.avg.value) + "</h6>");
        chart = bb.generate({
            data: {
                columns: columns,
                type: "bar",
                color: function (color, d) {
                    if (d.index === 0 && options.buildingNumber) {
                        color = "#355C54";
                    } else {
                      color = "#439686"
                    }
                    return color;
                }
            },
            bar: {
                width: {
                    ratio: 0.8
                },
                zerobased: true
            },
            axis: {
                x: {
                    type: "category",
                    categories: options.categories
                },
                y: {
                    max: 0.9
                }
            },
            grid: {
                y: {
                    lines: [
                        {
                            value: options.avg.value,
                            text: options.avg.label,
                            class: "fci-average"
                        }
                    ]
                }
            },
            bindto: "#fciBarChart"
        });
        chart.legend.hide();
        return chart;
    }

    function mungeBuildingData(buildingList) {
        // console.log(buildingList.length);
        var regionCheck = {};
        for (var i = 0; i < buildingList.length; i++) {
            //building Region Handling
            if (typeof(regionCheck[buildingList[i].campus_e_name]) == "undefined") {
                buildingRegions.push(buildingList[i].campus_e_name);
                regionCheck[buildingList[i].campus_e_name] = true;
                buildingsByRegion[buildingList[i].campus_e_name] = [];
                buildingsByRegion[buildingList[i].campus_e_name].push(buildingList[i]);
            } else {
                buildingsByRegion[buildingList[i].campus_e_name].push(buildingList[i]);
            }
            //building usage groups
            var usage = usageMap[buildingList[i].use.replace(/\s/g, "_")];
            if (typeof(buildingUseMap[usage]) == "undefined") {
                buildingUseMap[usage] = buildingList[i].use;
                buildingsByUse[usage] = [];
                buildingsByUse[usage].push(buildingList[i])
            } else {
                buildingsByUse[usage].push(buildingList[i])
            }
            // department groups
            if (buildingList[i].departments) {
                for (var j = 0; j < buildingList[i].departments.length; j++) {
                    if (typeof(buildingDept[buildingList[i].departments[j][0]]) == "undefined") {
                        buildingDept[buildingList[i].departments[j][0]] = buildingList[i].departments[j][1];
                        buildingDeptList[buildingList[i].departments[j][0]] = [];
                        buildingDeptList[buildingList[i].departments[j][0]].push(buildingList[i]);
                    } else {
                        buildingDeptList[buildingList[i].departments[j][0]].push(buildingList[i]);
                    }
                }//end buildingList for loop
            }
        }//end for loop


        //add regions to region select
        $("#buildingRegion").html(function () {
            //create two optgroups, one for Campus Regions:
            var options = "<optgroup label='Campus Regions' class='region_group'><option value=\"campus\">Campus</option>";
            for (var i = 0; i < buildingRegions.length; i++) {
                options += "<option value='" + buildingRegions[i] + "'>" + buildingRegions[i] + "</option>";
            }
            //and one for Building Usage
            options += "</optgroup><optgroup label='Building Usage' class='usage_group'>";
            var keyArray = Object.keys(usageMap), checker = {};
            for (var j = 0; j < keyArray.length; j++) {
                if (typeof(checker[ usageMap[ keyArray[j] ] ]) == "undefined") {
                    options += "<option value='" + usageMap[keyArray[j]] + "'>" + usageMap[keyArray[j]].replace(/_/g," ") + "</option>";
                    checker[ usageMap[keyArray[j] ] ] = true;
                }
            };
            options += "</optgroup>";
            return options;
        });

        // listen for changes in select
        $("#buildingRegion").on("change", function (event) { //"optgroup.regionGroup",
          var type = $("#buildingRegion option:selected").parent().attr("class");
            if (type == "region_group") {
              var region = $(event.target).val();
              if (region == "campus") {
                var rawData = campusAverage;
                } else {
                  var rawData = buildingsByRegion[region];
                }
              var groupLabel = $("#buildingRegion option:selected").text();
              if (groupLabel == "CSG Buildings") {
                groupLabel = "CSG";
              }
            } else {
              var usage = $(event.target).val();
              var rawData = buildingsByUse[usage];
              var groupLabel = $("#buildingRegion option:selected").text();
            }
            var colData = mungeBuildings(rawData);
            var options = {};
            options.categories = colData.cats;
            options.avg = {
                "value": d3.mean(colData.column),
                "label": "Average for " + groupLabel + " buildings (" + twoDecimalRound( d3.mean(colData.column) ) + ")"
            };
            options.groupLabel = groupLabel;
            colData.column.splice(0, 0, "Building FCI");
            chart.destroy();
            chart = buildFCIBarChart([colData.column], options);
        });
        buildCampusChart(buildingsByUse);
    }//end mungeBuildingData

    function mungeBuildings(buildingArray) {
      buildingArray.sort(function(a,b){
        return b.fci - a.fci;
      })
        var returnOb = {}
        returnOb.column = [];
        returnOb.cats = [];
        buildingArray.map(function (building) {
            returnOb.column.push(twoDecimalRound(building.fci));
            returnOb.cats.push(building.building_number);
        });
        return returnOb;
    }

    function twoDecimalRound(decimal) {
        return Math.round(decimal * 100) / 100;
    }

    function findUniqueVals(ob) {
        var retArray = [],keys = Object.keys(ob),checker = {};
        for (var i=0;i<keys.length;i++){
            if (typeof(checker[ob[keys[i]]]) == "undefined") {
                retArray.push(ob[keys[i]]);
                checker[ob[keys[i]]] = true;
            }
        }
        return retArray;
    }
    //create campus chart by averaging by usage
    function buildCampusChart(buildingsByUse) {
        for (var a = 0; a < uniqueUsages.length; a++) {
            var bldgArr = buildingsByUse[uniqueUsages[a]];
            var fciAverage = twoDecimalRound(
                d3.mean(bldgArr, function (d) {
                    return twoDecimalRound(d.fci)
                })
            );
            campusAverage.push({"fci": fciAverage, "building_number": uniqueUsages[a].replace(/_/g," ")});
        };
        var colData = mungeBuildings(campusAverage);
        var options = {};
        options.categories = colData.cats;
        options.avg = {
            "value": d3.mean(colData.column),
            "label": "Average of averaged usage for all Campus buildings (" + twoDecimalRound(d3.mean(colData.column)) + ")"
        };
        options.building = true;
        colData.column.splice(0, 0, "Building FCI");
        // chart.destroy();
        fcichart = buildFCIBarChart([colData.column], options);
    }
}//end fciBarChart function
