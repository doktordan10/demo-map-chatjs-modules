function mapLeaflet(){
  //beginning of responsive code, needs to hook to page resize
  var zoom = 17, height = 700;
  if ($("#map").width() < 800) {
    zoom = 15;
    height = 350;
  }
  $("#map").height(height);
    map = L.map('map', {
        center: [42.3606, -71.085],
        zoom: zoom,
        loadingControl: true,
        zoomControl: false
    });
    osm = L.tileLayer('https://api.mapbox.com/styles/v1/mapbox/light-v9/tiles/256/{z}/{x}/{y}?access_token=pk.eyJ1IjoiZG9rdG9yZGFuMTAiLCJhIjoiNTIzNzI5OTRhMzBhZTgwODU1Y2Q3MWIwNzU2Y2MwNDUifQ.FO_yXL-i1D0CB9FU8j-3nA', {
        maxZoom: 18,
        minZoom: 6
    }).addTo(map);

      var data = {
          "type": "FeatureCollection",
          "features": ""
      };

      data.features = [{
          "age": 14,
          "fci": 0.02412001,
          "size": "418300",
          "type": "Feature",
          "year": 2005,
          "geometry": {
          "type": "Polygon",
          "coordinates": [
            [
              [-71.09158125787981, 42.36265498302057],
              [-71.09128775700523, 42.36263295109149],
              [-71.09123764536977, 42.36265766236688],
              [-71.0912135668467, 42.36262736492153],
              [-71.09069550431647, 42.362588318021814],
              [-71.0912252336244, 42.362321249917635],
              [-71.09116692352244, 42.36225762816323],
              [-71.0913896172301, 42.36214536046858],
              [-71.09138450983278, 42.36213977134007],
              [-71.0914880787672, 42.362087562424975],
              [-71.09151471824156, 42.36211660670848],
              [-71.0915696205694, 42.36208892156681],
              [-71.09146291990139, 42.36197249725826],
              [-71.09185900641782, 42.36177281335684],
              [-71.09204180116016, 42.36197224102115],
              [-71.09194009028829, 42.36202349731916],
              [-71.09209720473767, 42.36219489872558],
              [-71.09212909064199, 42.36217884893534],
              [-71.09234650283655, 42.36241602008616],
              [-71.09187980004155, 42.362646984975974],
              [-71.09186380731629, 42.36265395227244],
              [-71.0918387639111, 42.36266440127199],
              [-71.09178213524034, 42.36267849776725],
              [-71.09172230325707, 42.36268407604195],
              [-71.09160417605902, 42.36268151756537],
              [-71.09158125787981, 42.36265498302057]
            ],
            [
              [-71.09143588105792, 42.36255825380293],
              [-71.09131186693742, 42.362620438135274],
              [-71.09150714298818, 42.3626364178547],
              [-71.09143588105792, 42.36255825380293]
            ],
            [
              [-71.09110451717837, 42.36260611490385],
              [-71.09103325577392, 42.36252795150231],
              [-71.09090924253677, 42.362590134504956],
              [-71.09110451717837, 42.36260611490385]
            ]
          ]
        },
        "properties": {
          "LAYER": "C-BLDG",
          "Address": "43 VASSAR ST",
          "FACILITY": "46",
          "BLDG_NAME": "BCSC",
          "Shape_Area": 67763.370972,
          "Shape_Leng": 1578.98416888
        }
      },{
        "age": 99,
        "fci": 0.05046253,
        "size": "150661",
        "type": "Feature",
        "year": 1920,
        "geometry": {
          "type": "Polygon",
          "coordinates": [
            [
              [-71.08783014621274, 42.36199190117864],
              [-71.08783656619433, 42.3619448535089],
              [-71.08776533760262, 42.36193950619682],
              [-71.08778125100463, 42.361822922770706],
              [-71.08800490747073, 42.36183994016522],
              [-71.08814799421728, 42.36185068081795],
              [-71.08806585592633, 42.36245249588064],
              [-71.08777030535265, 42.36243031168194],
              [-71.08783014621274, 42.36199190117864]
            ]
          ]
        },
        "properties": {
          "LAYER": "C-BLDG",
          "Address": "400  MAIN ST",
          "FACILITY": "E19",
          "BLDG_NAME": "FORD BUILDING (E19)",
          "Shape_Area": 18524.7693748,
          "Shape_Leng": 640.223185255
        }
      }];
  
      var myStyle = {
          "color": "#3733C4",
          "weight": 0,
          "fillOpacity": 0.5
      };
      var hoverStyle = {
          "color": "#3733C4",
          "fillOpacity": 0.8
      };
      var clickStyle = {
          "color": "green",
          "opacity": 1
      };
      var style1={
          "color":"#161AAC",
          "fillOpacity": 0.5
      };


      function highlight(layer) {
          originalColor = layer.options.color;
          originalOpacity = layer.options.fillOpacity;
          layer.setStyle(hoverStyle);
          if (!L.Browser.ie && !L.Browser.opera) {
              layer.bringToFront();
          }
      }

      function dehighlight(layer) {
          if (selected === null || selected._leaflet_id !== layer._leaflet_id) {
              layer.setStyle({"color":originalColor,
                              "fillOpacity":originalOpacity});
          } else {
              layer.setStyle(clickStyle);
              if (!L.Browser.ie && !L.Browser.opera) {
                  layer.bringToFront();
              }
          }

      }

      function select(layer) {
          var buildingvar = layer.feature.properties.FACILITY;
          if (buildingvar != ""){
              buildingvar = "buildingdetail/"+buildingvar;
              goToURL(buildingvar);
          }
      }

          myLayer = L.geoJSON(data, {
          style: function (feature) {
              return myStyle
          },
          onEachFeature: function (feature, layer) {
              polygons1[layer.feature.properties.FACILITY] =  layer;
              layer.on({
                  'mouseover': function (e) {
                      highlight(e.target);
                  },
                  'mouseout': function (e) {
                      dehighlight(e.target);
                  },
                  'click': function (e) {
                      select(e.target);
                  }
              });
              layer.bindTooltip(
                  `
                  <div class="container p-0" style="width:250px;">
                    <div class="row no-gutters title text-center">
                      <div class="col-sm-12 ">
                        <strong>${(layer.feature.properties.FACILITY).toUpperCase()} – ${toTitleCase(layer.feature.properties.BLDG_NAME)}</strong>
                      </div>
                      <div class="col-sm-12">${toTitleCase(layer.feature.properties.Address)}</div>
                    </div>

                    <div class="row no-gutters">
                      <div class="col-sm-12">
                        <img class="" src="https://www.washingtonpost.com/resizer/I9IJifRLgy3uHVKcwZlvdjUBirc=/1484x0/arc-anglerfish-washpost-prod-washpost.s3.amazonaws.com/public/ZQIB4NHDUMI6RKZMWMO42U6KNM.jpg" width="400"  alt="" style="max-width: 100%; height:auto;">
                      </div>
                    </div>
                    <dl class="row pl-2 pr-2 pt-2 mb-0">

                      <dt class="col-sm-2">Age</dt>
                      <dd class="col-sm-4">${layer.feature.age}</dd>

                      <dt class="col-sm-2">Year</dt>
                      <dd class="col-sm-4">${layer.feature.year}</dd>

                      <dt class="col-sm-2">FCI</dt>
                      <dd class="col-sm-4">${layer.feature.fci.toFixed(2)}</dd>

                      <dt class="col-sm-2">Size</dt>
                      <dd class="col-sm-4">${layer.feature.size.toString().replace(/\B(?=(\d{3})+(?!\d))/g, ",") + " ft²"}</dd>
                    </dl>
                  </div>
                  `);

              map.on({
                  'click': function (e) {
                      // myLayer.resetStyle(layer);
                      //   selected = null;
                  }
              });
          }
      }).addTo(map);


  function toTitleCase(str) {
      if (str != null) {
          return str.replace(/\w\S*/g, function (txt) {
              return txt.charAt(0).toUpperCase() + txt.substr(1).toLowerCase();
          });
      } else {
          return '';
      }
  }
}
