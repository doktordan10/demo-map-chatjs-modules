function filterButtons(){
	var myStyle = {
        "color": "#3733C4",
        "weight": 0,
        "fillOpacity": 0.1
    };
    var hoverStyle = {
        "color": "#3733C4",
        "fillOpacity": 0.5
    };
    var clickStyle = {
        "color": "green",
        "opacity": 1
    };
    var style1={
        "color":"#161AAC",
        "fillOpacity": 0.5
    };
    $("#fciButton").on("click",function(event){
        goToURL("fci");
    });
	 $("input[type='checkbox']").change(function() {
	      myLayer.setStyle(myStyle);
	    if(this.checked) {
	       filterParams += $(this).val() + ',';
	    }else{
	       filterParams = filterParams.replace($(this).val() + ',','');
	    }
	    $.ajax({
	        type: 'GET',
	        url: 'https://naioya5bdl.execute-api.us-east-1.amazonaws.com/dev/buildingfilter',
	        dataType: 'json',
	        data:{program_status:filterParams.replace(/,\s*$/, "")}
	    }).done(function (json) {
	        var centerpan = [];
	        $.each(json.projects, function(k, v) {
	            if (v["asset_number"] != ""){
	                v["asset_number"] = v["asset_number"].replace('.0','');
	               if (polygons1[v["asset_number"]]!=undefined){
	                var lat = polygons1[v["asset_number"]]["feature"]["geometry"]["coordinates"][0][0][1];
	                var longi = polygons1[v["asset_number"]]["feature"]["geometry"]["coordinates"][0][0][0]
	                    centerpan.push([lat,longi]);
	                    polygons1[v["asset_number"]].setStyle(style1);
	                    if (!L.Browser.ie && !L.Browser.opera) {
	                        polygons1[v["asset_number"]].bringToFront();
	                    }
	               }
	            }

	        });
	        var bounds = new L.LatLngBounds(centerpan);
	       	if (bounds._southWest){
	       		map.fitBounds(bounds);
	       	}
	    });
	});
}
